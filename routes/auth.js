const express = require("express");
const router = express.Router();
const auth = require("../server/auth");
const middleware = require("../server/middleware");

router.post("/register", auth.register);

router.post("/login", auth.login);

router.post("/isLogged", middleware.isLogged, auth.isLogged);

router.get("/logout", middleware.isLogged, auth.logout);

module.exports = router;