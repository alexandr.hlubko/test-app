const express = require("express");
const router = express.Router();
const user = require("../server/user");
const middleware = require("../server/middleware");

router.post("/getUserInfo", middleware.isLogged, user.getUserInfo);

router.post("/getCity", middleware.isLogged, user.getCity);

router.post("/getState", middleware.isLogged, user.getState);

router.post("/updateUserInfo", middleware.isLogged, user.updateUserInfo);


module.exports = router;