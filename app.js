const express = require("express");
const app = express();
const passport = require("passport");
const session = require("express-session");
const bodyParser = require("body-parser");
const path = require("path");
const routerAuth = require("./routes/auth.js");
const routerUser = require("./routes/user.js");

app.use(express.static(path.join(__dirname, "client/dist")));
app.use(express.static(path.join(__dirname, "public")));
app.use(bodyParser.json());
app.use(session({ secret: "key", saveUninitialized: false, resave: false }));
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(passport.initialize());
app.use(passport.session());

app.use("/api/auth/", routerAuth);

app.use("/api/user/", routerUser);

app.get("/*", function(req, res) {
    res.sendFile(__dirname + "/client/dist/index.html");
});

app.listen("3000", () => {
    console.log("Run 3000 ports");
});