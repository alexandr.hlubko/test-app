const obj = {};
const axios = require("axios");
const db = require('./database');


obj.isLogged = function(req, res, next) {
    req.isAuthenticated() ? next() : res.status(401);
};

// Add to database
function addState(data) {
    const addState = new db.State(data);
    addState.save(function(err) {
        if (err) {
            console.log(err);
        } else {
            console.log('Success');
        }
    });
}

function addCity(city) {
    const addCity = new db.Сity(city);
    addCity.save(function(err) {
        if (err) {
            console.log(err);
        } else {
            console.log('Success');
        }
    });
}

// Parse https://api.hh.ru/areas/113

// axios.get('https://api.hh.ru/areas/113').then(res => {
//     // console.log(res.data.areas);
//     const arrays = [];
//     res.data.areas.forEach(element => {
//         // addState({
//         //     id: element.id,
//         //     name: element.name
//         // });
//         element.areas.forEach(item => {
//             addCity({
//                 parent_id: item.parent_id,
//                 name: item.name
//             });
//         });
//     });

//     console.log(arrays);
// }).catch(error => {
//     console.log(error);
// });

module.exports = obj;