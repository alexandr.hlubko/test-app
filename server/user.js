const db = require("./database");
const bcrypt = require("bcrypt");
const user = {};

user.getUserInfo = function(req, res) {
    const { name } = req.body;
    db.User.findOne({ username: name }).select('username email city state').exec(function(err, user) {
        if (err) {
            res.status(500).send(err.message);
        } else {
            res.status(200).send({ user });
        }
    });
};

user.getCity = function(req, res) {
    db.Сity.find({ parent_id: req.body.id }, function(err, data) {
        if (err) {
            res.status(500).send(err.message);
        } else {
            res.status(200).send({ data });
        }
    });
}



user.getState = function(req, res) {
    db.State.find({}, function(err, data) {
        if (err) {
            res.status(500).send(err.message);
        } else {
            res.status(200).send({ data });
        }
    });
}

user.updateUserInfo = function(req, res) {
    console.log(req.body);
    const { username, email, new_pass, password, city, state } = req.body.user;
    db.User.findOne({ username: req.body.name }, function(err, user) {
        if (err) {
            return err;
        }
        console.log(user);
        bcrypt.compare(password, user.password, function(err, result) {
            if (result) {
                bcrypt.hash(new_pass, 10, function(err, hash) {
                    db.User.updateOne({ username: req.body.name }, { username, email, password: hash, city, state },
                        function(err, data) {
                            if (err) {
                                res.status(500).send({ err: err.message });
                            } else {
                                res.status(200).send({ success: true });
                            }
                        }
                    );
                });
            } else {
                res.status(500).send("Старый пароль неверный");
            }
        });
    });
};

module.exports = user;