const mongoose = require("mongoose");

mongoose.connect("mongodb://localhost/TestProject", {
    useCreateIndex: true,
    useNewUrlParser: true
});

const userShema = new mongoose.Schema({
    username: { type: String, unique: true, required: true },
    password: { type: String, required: true },
    email: { type: String, unique: true, required: true },
    city: { type: String },
    state: { type: String }
});

const cityShema = new mongoose.Schema({
    parent_id: { type: Number, required: true },
    name: { type: String, required: true }
});

const stateShema = new mongoose.Schema({
    id: { type: Number, required: true },
    name: { type: String, required: true }
});

module.exports = {
    User: mongoose.model("User", userShema),
    Сity: mongoose.model("Сity", cityShema),
    State: mongoose.model("State", stateShema)
};