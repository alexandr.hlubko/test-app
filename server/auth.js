const passport = require("passport");
const LocalStrategy = require("passport-local");
const db = require("./database");
const bcrypt = require("bcrypt");
const auth = {};

auth.login = function(req, res, next) {
    passport.authenticate("local", function(err, user, info) {
        if (err) {
            res.status(500).send(err);
        } else if (user) {
            req.logIn(user, function(err) {
                return err ?
                    res.status(500).send("Incorrect Login") :
                    res.status(200).send({ user });
            });
        } else {
            res.status(500).send("Incorrect username or password");
        }
    })(req, res, next);
};

auth.register = function(req, res, next) {
    const { username, password, email } = req.body.user;
    bcrypt.hash(password, 10, function(err, hash) {
        const user = new db.User({ username, password: hash, email });
        user.save(function(err) {
            if (err) {
                res.status(500).send(err.message)
            } else {
                req.logIn(user, function(err) {
                    if (err) {
                        res.status(500).send({ erorr: err });
                    } else {
                        res.status(200).send({ success: true });
                    }
                });
            }
        });
    });
};

auth.isLogged = function(req, res, next) {
    if (req.isAuthenticated()) {
        res.status(200).send({ user: req.user });
    } else {
        res.status(401);
    }
};

auth.logout = function(req, res) {
    console.log('Auth logout');
    req.logout();
    res.redirect("/");
};

passport.use(
    new LocalStrategy(function(username, password, done) {
        db.User.findOne({ username }, function(err, user) {
            if (err) {
                return done(err);
            }
            if (!user) {
                return done(null, false);
            }
            bcrypt.compare(password, user.password, function(err, res) {
                return res ? done(null, user) : done(null, false);
            });
        });
    })
);

passport.serializeUser(function(user, done) {
    done(null, user.id);
});

passport.deserializeUser(function(id, done) {
    db.User.findById(id, function(err, user) {
        done(err, user);
    });
});

module.exports = auth;