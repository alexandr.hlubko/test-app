import Vue from "vue";
import Vuex from "vuex";
import VueResource from "vue-resource";

Vue.use(Vuex);
Vue.use(VueResource);

const user = JSON.parse(localStorage.getItem("user"));


const store = new Vuex.Store({
    state: {
        user: user ? user : null,
    },
    mutations: {
        login(state, user) {
            state.user = user;
        },
        logout(state) {
            state.user = null;
        },
        setUserInfo(state, user) {
            state.user = user;
        }
    },
    actions: {
        logged({ commit }, user) {
            commit("login", user);
            localStorage.setItem("user", JSON.stringify(user));
        },
        logout({ commit }) {
            commit("logout");
            localStorage.removeItem("user");
        },
        setUserInfo({ commit }, user) {
            commit("setUserInfo", user);
            localStorage.setItem("user", JSON.stringify(user));
        }
    }
});

export default store;