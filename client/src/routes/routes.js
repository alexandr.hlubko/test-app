import VueRouter from "vue-router";
import store from "../store/store";
import Login from "../components/Login";
import Register from "../components/Register";
import MainHome from "../components/Main/Home";
import NoPage from "../components/NoPage";

const router = new VueRouter({
    routes: [{
            path: "/",
            redirect: "/login"
        },
        {
            path: "/login",
            component: Login,
            meta: {
                guest: true
            }
        },
        {
            path: "/register",
            component: Register,
            meta: {
                guest: true
            }
        },
        {
            path: "/main",
            component: MainHome,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: "*",
            component: NoPage
        }
    ]
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (!store.state.user) {
            next({
                path: "/login"
            });
        } else {
            next();
        }
    } else if (to.matched.some(record => record.meta.guest)) {
        if (store.state.user) {
            next({
                path: "/main"
            });
        } else {
            next();
        }
    } else {
        next();
    }
});

export default router;